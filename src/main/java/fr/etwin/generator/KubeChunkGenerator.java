package fr.etwin.generator;

import fr.etwin.utils.*;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.generator.BiomeProvider;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.generator.WorldInfo;

import java.util.*;

public class KubeChunkGenerator extends ChunkGenerator {
    private final Map<Coordinate, KubeChunk> chunkCache;
    private final KubeChunkProvider kProvider;

    public KubeChunkGenerator(KubeChunkProvider kProvider) {
        this.chunkCache = new CacheHashMap<>(15);
        this.kProvider = kProvider;
    }

    @Override
    public void generateSurface(WorldInfo worldInfo, Random random, int mX, int mY, ChunkData chunk) {
        Map.Entry<Coordinate, Coordinate> coords = KubeConstants.getCoordsFromMinecraftChunk(mX, mY);
        Coordinate kubeChunkCoords = coords.getKey();
        Coordinate minecraftChunkInsideKubeChunkCoords = coords.getValue();

        System.out.printf("Request chunk %s:%s => %s / %s%n", mX, mY, kubeChunkCoords, minecraftChunkInsideKubeChunkCoords);

        KubeChunk kChunk;
        if (chunkCache.containsKey(kubeChunkCoords)) {
            kChunk = chunkCache.get(kubeChunkCoords);
        } else {
            kChunk = kProvider.getChunk(kubeChunkCoords);
            chunkCache.put(kubeChunkCoords, kChunk);
            if (kChunk == null) {
                System.out.println("Chunk seems empty...");
            }
        }

        if (kChunk == null)
            return;

        int offsetX = minecraftChunkInsideKubeChunkCoords.mX * MinecraftConstants.CHUNK_WIDTH;
        int offsetZ = minecraftChunkInsideKubeChunkCoords.mY * MinecraftConstants.CHUNK_WIDTH;

        for (int X = 0; X < 16; X++) {
            for (int Y = 0; Y < 32; Y++) {
                for (int Z = 0; Z < 16; Z++) {
                    KubeBlock kBlock = kChunk.getBlock(X + offsetX, Y, Z + offsetZ);
                    if (kBlock != null) chunk.setBlock(X, Y, Z, kBlock.minecraftMaterial);
                }
            }
        }
        for (int X = 0; X < 16; X++) {
            for (int Z = 0; Z < 16; Z++) {
                chunk.setBlock(X, -1, Z, Material.BEDROCK);
            }
        }
    }

    @Override
    public Location getFixedSpawnLocation(World world, Random random) {
        return new Location(world, 0, 1, 0);
    }

    @Override
    public BiomeProvider getDefaultBiomeProvider(WorldInfo worldInfo) {
        return new KubeBiomeProvider();
    }
}