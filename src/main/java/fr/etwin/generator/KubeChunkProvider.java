package fr.etwin.generator;

import fr.etwin.utils.Coordinate;
import fr.etwin.utils.KubeChunk;

public interface KubeChunkProvider {
    public default KubeChunk getChunk(Coordinate coords) {
        return getChunk(coords.mX, coords.mY);
    }

    public KubeChunk getChunk(int mX, int mY);
}
